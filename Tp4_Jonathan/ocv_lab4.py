import sys
import cv2
import numpy as np
import os
#img
i = 3
j = 3 
img = cv2.imread(sys.argv[1])
imgMedianBlur = cv2.medianBlur(img, i)
imgLisse = cv2.blur(img, (i, j))
imgGaussien = cv2.GaussianBlur(img, (i, j), .75)

close= 0
cv2.imshow('Original', img)

while close == 0:

    cv2.imshow('Filtre moyen', imgLisse)
    cv2.imshow('Filtre de Mediane', imgMedianBlur)
    cv2.imshow('Gaussien', imgGaussien)

    if cv2.waitKey(4) & 0xFF == 27: 
        close = 1
    
    if cv2.waitKey(4) & 0xFF == ord('-'):
      if j >= 3 and i >= 3 :
        i -= 2
        j -= 2
      imgMedianBlur = cv2.medianBlur(img, i)
      imgLisse = cv2.blur(img, (i, j))
      imgGaussien = cv2.GaussianBlur(img, (i, j), .75*(i+j))
      

    if cv2.waitKey(4) & 0xFF == ord('='):
      i += 2
      j += 2
      imgMedianBlur = cv2.medianBlur(img, i)
      imgLisse = cv2.blur(img, (i, j))
      imgGaussien = cv2.GaussianBlur(img, (i, j), .75*(i+j))
      
        
cv2.destroyAllWindows()